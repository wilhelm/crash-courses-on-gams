# Crash course on GAMs



## Part 1: a theoretical primer

* LM as a modelization of the expectation: $`Y = \mathbf{X}\boldsymbol{\beta}  + \boldsymbol{\varepsilon}, \quad \boldsymbol{\varepsilon} \sim \mathcal{N}(\mathbf{0}, \sigma^2 \mathbf{I})`$ is equivalent to $`Y \sim \mathcal{N}(\mathbf{X}\boldsymbol{\beta}, \sigma^2 \mathbf{I})`$, which is equivalent to $`Y`$ is normally distributed and $`\text{E}[Y| \mathbf{X}] = \mathbf{X}\boldsymbol{\beta} `$
* GLM is a slight generalization of this.  $`Y`$ follows a distribution within the [exponential family](https://en.wikipedia.org/wiki/Exponential_family) and $`\text{E}[Y| \mathbf{X}] = \mathbf{X}\boldsymbol{\beta} `$.
* GAM then essentially amounts to estimating _functions_ and not _coefficients_ as in the linear case.
* Interpolating curves
    * Necessity of adding penalization for having a well defined problem 
    * Interpolation and relation to noise in the estimation
* Smoothing: 
    * Definition
    * Measure (wiggliness and squared of derivative)
* GAM fitting
    * Penalized log-likelihood
    * smoothing parameter as bias/variance tradeoff

* smoothing parameter choice
    * Illustration
    * Approximation of the MSE as a criterion and cross-validation
* Basis functions
    * Basis expansion
    *  Number of basis functions
* Conclusion


## Part 2: practical aspects

* A review of existing tools
    * `mgcv` and relatives 
    * Reminder about the structure of GAM
    * Elements of GAMs: parametric terms, smooth terms ad possibly random effects
* Model specification
    * Number of basis functions
    * Effective degrees of freedom
    * Sum-to-0 constraint
    * Use of the `predict.gam` function
* Factor-smooth interaction and ANOVA
    * Specification with `by = ` argument
    * Smooth terms with `ti()`
    * Diagnostic plots
* Some practical aspects
    * Flexible $`\neq`$ Magic
    * Biological interpretation
* Side notes
    * Bayesian interpretation
    * GAMs and relatives (and their implementation): `qgam`, `gaulss` etc.
* Conclusion
* Through modelling: Yeast example
    * look at the data
    * modelling of the time dependance
    * other factors
    * predictive power of the model (train/test split and checks of the coverage rates of the model)
* diagnostics (deviance, residuals, other useful plots)
* assesment of the uncertainty: Cis, and Bayesian credible intervals etc.
* talk about spatial modelling with GAMs
* extensions of GAMs: Generalized additive models for location, scale and shape (wich includes: smoothly varying variance):
    * `gauls` (Gaussian location, scale and shape);
    * `gammals` (Gamma location, scale and shape)
* post-selection inference
* pacakges: mgcv, migViz, gratia, itasdug etc.
* references: Wood (2003, 2008), Soap film, Fast stable REML for semiparametrice GLM (JRSSB, 2011), Wood and Marra (2012, Scan. Journ. Stat.)
* other resources (website of online course, book of Wood etc.)


# Some ressources on the web

* A [free online course](https://noamross.github.io/gams-in-r-course/), with exercises for gam fitting with `mgcv`. Learning by practicing is extremely efficient!
* Beautiful [slides](https://github.com/eco4cast/Statistical-Methods-Seminar-Series/tree/main/simpson-gams) of Gavin Simpson about basics of GAM's, with some hints and beautiful [animations](https://github.com/eco4cast/Statistical-Methods-Seminar-Series/blob/main/simpson-gams/slides/gams.Rmd).
* A  [course](https://noamross.github.io/mgcv-esa-workshop/) on GAM's (slightly old, but interesting)
* More generally, the great book [The Elements of Statistical Learning](https://hastie.su.domains/ElemStatLearn/) as well as one [Solution manual](https://www.waxworksmath.com/Authors/G_M/Hastie/WriteUp/Weatherwax_Epstein_Hastie_Solution_Manual.pdf) may prove to be useful
* Gavin Simpson' [blog](https://fromthebottomoftheheap.net/)
* The book definitive [book](https://www.taylorfrancis.com/books/mono/10.1201/9781315370279/generalized-additive-models-simon-wood) by Simon N. Wood. Contact me if you need a pdf version of it.
