##### Plot B-splines basis functions #####
library(fda)
library(mgcv)
library(tidyverse)
library(gganimate)
library(gifski)
library(magick)

################################################ 
########## Illustration of smoothness ##########
################################################ 

n = 100 +1
x = seq(0,1,length = n)
lag.dist = n

fx = function(x, a = 0, b = 0)   a*x + b - (1/(4*pi^2)) *sin(2*pi*x)
sq.sec.deriv <- function(x)(sin(2* pi * x))^2



set.seed(1)
sd.seq = rev(c(0,0.02, 0.05, 0.1,0.15,0.2))
funtest <- function(z)list(data.frame(x = x, y = fx(x) + rnorm(n, sd = z)/50, sd = z))
df.plot <- do.call(rbind, sapply(sd.seq ,funtest))



df.plot <- df.plot %>% group_by(sd) %>%
  summarise(IntF2 = round(integrate(function(t){(splinefun(x, y)(t, deriv = 2))^2}, lower = 0, upper = 1, subdivisions = 10^6)$value, digits = 1), sd = sd) %>% 
  distinct %>% 
  ungroup %>%
  right_join(x = df.plot, y = ., by = "sd")
df.plot <- df.plot %>% mutate(sd = factor(sd, levels = sd.seq), state = IntF2)



## finer and smoother interpolation
lengthout <- 5*n + 2
fun.extrapolate <- function(z){
  sp.df <- spline(x = df.plot %>% filter(sd == z) %>% pull(x),
                  y = df.plot %>% filter(sd == z) %>% pull(y), 
                  xout = seq(0,1, length = lengthout))
  sp.df$sd <- rep(z, length = lengthout)
  ret <- do.call(cbind, sp.df)
  ret
}
df.plot2 <- do.call(rbind, lapply(sd.seq ,fun.extrapolate)) %>% as.data.frame() %>% mutate(sd = factor(sd, levels = sort(unique(sd), decreasing = T)))
df.plot2 <- right_join(x = df.plot2, y =df.plot %>%
                         dplyr::select(sd, IntF2) %>%
                         distinct() %>%
                         mutate(Log = round(log(IntF2 + 1), digits = 2)) %>%
                         mutate(Log = factor(Log, levels = sort(unique(Log), decreasing = T))), by = "sd" )





p <- ggplot(df.plot2, aes(x = x, y = y)) + 
  geom_line() +
  geom_line(mapping = aes(x = x, y = y, col = "smooth function"), data = data.frame(x = x, y = fx(x)), inherit.aes = F) +
  scale_color_discrete("") +
  theme(text = element_text(size = 20))


animSmooth <- animate(p +
                        labs(title = "Increasingly smoother functions", subtitle = "Smoothness index: {closest_state}") +
                        transition_states(Log,
                                          transition_length = 2,
                                          state_length = 1),
                      width = 800, height = 500
)
anim_save(animSmooth, file = "graphs/smoothness_anim.gif") 
save(df.plot2, file = "dfPlot_smoothness.RData")



ggplot(df.plot.indbasis, aes(x = x, y = y)) +
  geom_path(col = df.plot.indbasis$col) +
  geom_point(data = df.data, mapping = aes(x = x.test, y = y, shape = "observed values"),  inherit.aes = F) +
  geom_line(data = df.truefun, mapping = aes(x.test, f, col = "true function"), inherit.aes = F)+
  scale_color_manual("", breaks = waiver(), labels =c("estimated function","True function" ), values = c("black", "red")) +
  geom_path(data = df.plot.sumbasis %>% filter(state == "fitted"), aes(x = x, y = y, col ="red"), inherit.aes = F)





####
################################################ 
######### Varying smoothing parameter #########
################################################ 

#### Varying smoothing parameter, short and long version
f <- function(x) 0.25 * x^11 * (10 * (1 - x))^6 + 6 * (10 * x)^3 *  (1 - x)^10 + 1 + sin(x * 12 * pi)+ 0.1
n = 100
set.seed(1234)
e <- rnorm(n, 0, sd = 0.7)
x.test <- seq(0,1,length  = n)

fx <- f(x.test)
y <- fx+ e
df.data <- data.frame(x.test = x.test, y = y)
k.basis <- 98

fit0 <- gam(formula = y~ s(x.test, k = k.basis), data = df.data)
powers <- 10^c(-7:6)
lambdas.seq <- fit0$sp * powers

neval = 1000
x.grid <- seq(0,1, l = neval)

fun.vary <- function(lam)
{
  fit <- gam(formula = y~ s(x.test, k = k.basis), data = df.data, sp = lam)
  fac = powers[which(lam == lambdas.seq)]
  data.frame(x = x.grid, y = predict(fit, newdata = data.frame(x.test  = x.grid)), fac = rep(fac, length(x.grid)) )
}

df.vary.sp <- do.call(rbind, lapply(lambdas.seq, fun.vary)) %>% tibble

df.truefun <- data.frame(x.test = x.grid, f = f(x.grid) )



ggplot(data = df.vary.sp, mapping = aes(x = x, y = y, col = as.factor(fac))) +
  geom_point(mapping = aes(x = x.test, y = y), data = df.data, inherit.aes = F) +
  geom_line()





line.size = 1
anim <- animate(
  ggplot(data = df.vary.sp, mapping = aes(x = x, y = y,col = "Smoothed function")) +
    geom_point(mapping = aes(x = x.test, y = y), data = df.data, inherit.aes = F) +
    geom_line(size = line.size) +
    scale_linetype_discrete("") +
    theme(text = element_text(size = 20)) +
    geom_line(data = df.truefun, mapping = aes(x.test, f, linetype = "True function"), inherit.aes = F, size = line.size)+
    geom_line(data = df.vary.sp %>% filter(fac == 1) %>% dplyr::select(-fac), mapping = aes(x =x, y = y, col = "Estimated function"), inherit.aes = F, size = line.size) +
    scale_color_manual("", breaks = c("Smoothed function", "Estimated function"), values =  scales::hue_pal()(5)[c(1,4)],) +
    # scale_color_discrete("factor") +
    labs(title = "Varying smoothing parameter", subtitle = "mult. factor w.r.t. estimated: {closest_state}") +
    transition_states(as.factor(fac), transition_length = 3, state_length = 1,
                      wrap = FALSE)+
    ease_aes('cubic-in-out'),
  duration = 8,
  width = 800, height = 500,
  renderer =  gifski_renderer(loop = F)
)

anim_save('graphs/varying_smoothing_param_noloop.gif', anim)
anim <- animate(
  ggplot(data = df.vary.sp, mapping = aes(x = x, y = y,col = "Smoothed function")) +
    geom_point(mapping = aes(x = x.test, y = y), data = df.data, inherit.aes = F) +
    geom_line(size = line.size) +
    scale_linetype_discrete("") +
    theme(text = element_text(size = 20)) +
    geom_line(data = df.truefun, mapping = aes(x.test, f, linetype = "True function"), inherit.aes = F, size = line.size)+
    geom_line(data = df.vary.sp %>% filter(fac == 1) %>% dplyr::select(-fac), mapping = aes(x =x, y = y, col = "Estimated function"), inherit.aes = F, size = line.size) +
    scale_color_manual("", breaks = c("Smoothed function", "Estimated function"), values =  scales::hue_pal()(5)[c(1,4)],) +
    # scale_color_discrete("factor") +
    labs(title = "Varying smoothing parameter", subtitle = "mult. factor w.r.t. estimated: {closest_state}") +
    transition_states(as.factor(fac), transition_length = 3, state_length = 1,
                      wrap = FALSE)+
    ease_aes('cubic-in-out'),
  duration = 8,
  width = 800, height = 500,
  renderer =  gifski_renderer(loop = T)
)
anim_save('graphs/varying_smoothing_param.gif', anim)




################################################ 
#### Illustration of the process of fitting ####
################################################ 

### 
f <- function(x) 0.25 * x^11 * (10 * (1 - x))^6 + 6 * (10 * x)^3 *  (1 - x)^10 + 1 + sin(x * 12 * pi)+ 0.1
n = 100
set.seed(1234)
e <- rnorm(n, 0, sd = 0.7)
# x.test <- sort(runif(n))
x.test <- seq(0,1,length  = n)

fx <- f(x.test)
y <- fx+ e

df.data <- data.frame(x.test = x.test, y = y)
###
f <- function(x) 0.25 * x^11 * (10 * (1 - x))^6 + 6 * (10 * x)^3 *  (1 - x)^10 + 1 + sin(x * 12 * pi)+ 0.1
n = 100
set.seed(1234)
e <- rnorm(n, 0, sd = 0.7)
# x.test <- sort(runif(n))
x.test <- seq(0,1,length  = n)

fx <- f(x.test)
y <- fx+ e
curve(f, col = "red", ylim = range(c(y,fx)), n = 3*n)
points(x.test, y, col = "blue")
df.data <- data.frame(x.test = x.test, y = y)


fit.nb <- function(nb, x.grid =seq(0,1,l = 300) )
{
  bspl <- create.bspline.basis(norder=3, breaks=seq(from =0,to =1, length= nb-1))
  coef <- diag(rep(1, nb))
  des.mat <- eval.fd(evalarg = x.test ,  fdobj =  fd(coef, bspl))
  fitted.coef <- lm(y ~ des.mat -1)$coef
  curve(f, col = "black", ylim = range(c(y,fx)), n = 3*n)
  points(x.test, y, col = "blue")
  y.fitted <- eval.fd(x.grid,  fd(fitted.coef, bspl))
  MSE <- round(mean((f(x.grid) - y.fitted)^2), digits = 3)
  lines(x.grid, y.fitted, col = "blue")
  legend("topright", legend = paste0("MSE = ", MSE))
  return(data.frame(nbasis = nb,x.grid = x.grid, fitted = y.fitted, MSE =MSE))
}

df.plot <- do.call(rbind, lapply(3:30, function(z)fit.nb(nb = z)))
df.plot$nbasis <- factor(df.plot$nbasis)
df.truefun <- data.frame(x.test = sort(unique(df.plot$x.grid)), f = f( sort(unique(df.plot$x.grid))) )


nb = 14
bspl <- create.bspline.basis(norder=3, breaks=seq(from =0,to =1, length= nb-1))
coef <- diag(rep(1, nb))
neval = 1000
x.grid  = seq(0,1, l = neval)



df.plot.indbasis <- eval.fd(evalarg = x.grid ,  fdobj =  fd(coef, bspl)) %>%
  as.data.frame() %>% 
  stack %>% 
  cbind(x.grid, ., "unfitted") %>%
  set_names(nm = c("x", "y", "basis fun.", "state")) %>% 
  mutate(y = na_if(y, 0))%>% 
  tibble

lookup.table.col <- df.plot.indbasis %>% dplyr::select(`basis fun.`) %>%
  unique %>% mutate(col =c( scales::hue_pal()(nb + 4)[3:(nb+2)]))

df.plot.indbasis <- left_join(df.plot.indbasis, lookup.table.col, by = "basis fun.")

des.mat <- eval.fd(evalarg = x.test ,  fdobj =  fd(coef, bspl))
fitted.coef <- diag(lm(y ~ des.mat -1)$coef)

df.plot2 <- rbind(df.plot.indbasis, eval.fd(evalarg = x.grid ,  fdobj =  fd(fitted.coef, bspl)) %>%
                    as.data.frame() %>% 
                    stack %>% 
                    cbind(x.grid, ., "fitted") %>%
                    set_names(nm = c("x", "y", "basis fun.", "state")) %>% 
                    mutate(y = na_if(y, 0))%>% 
                    tibble %>% 
                    left_join(., lookup.table.col, by = "basis fun.")
)
df.plot2$state <- factor(df.plot2$state, levels = c("unfitted", "fitted"))

ggplot(df.plot.indbasis, aes(x = x, y = y, col = `basis fun.`)) +
  geom_line()


coef <- rep(1, nb)
neval = 1000
x.grid  = seq(0,1, l = neval)
df.plot.sumbasis <- cbind(x.grid, eval.fd(evalarg = x.grid ,  fdobj =  fd(coef, bspl)) )  %>%
  as.data.frame() %>% 
  cbind(., "unfitted") %>%
  set_names(nm = c("x", "y", "state")) %>%
  tibble

df.plot.sumbasis <- rbind(df.plot.sumbasis,  cbind(x.grid, eval.fd(evalarg = x.grid ,  fdobj =  fd(diag(fitted.coef), bspl)) )  %>%
                            as.data.frame() %>% 
                            cbind(., "fitted") %>%
                            set_names(nm = c("x", "y", "state")) %>%
                            tibble)




anim <- animate(
  ggplot(df.plot2, aes(x = x, y = y)) +
    geom_path(col = df.plot2$col) +
    geom_point(data = df.data, mapping = aes(x = x.test, y = y, shape = "observed values"),  inherit.aes = F) +
    scale_shape_discrete("")+
    geom_line(data = df.truefun, mapping = aes(x.test, f, col = "true function"), inherit.aes = F)+
    scale_color_manual("", breaks = waiver(),
                       labels =c("estimated function","True function" ), 
                       values = c("black", "red")) +
    geom_path(data = df.plot.sumbasis, aes(x = x, y = y, col ="red"), inherit.aes = F) +
    theme(text = element_text(size = 20)) +
    labs(title = "{closest_state}") +
    transition_states(state, transition_length = 3, state_length = 1,
                      wrap = FALSE)+
    ease_aes('cubic-in-out'),
  nframes = 50,
  width = 800, height = 500
)
anim_save('graphs/fitting_splines_basis_fun.gif', anim)


## make the animation shorter (limiting the number of loops),
## using magick::image_animate
gif <- image_read("graphs/fitting_splines_basis_fun.gif")
# this is the original gif
gif2 <- image_animate(gif, loop = 2)
# gif that will loop only two times
image_write(gif2, "graphs/fitting_splines_basis_fun_short.gif")


#####################################################
### Illustration of the number of basis functions ###
#####################################################



f <- function(x) 0.25 * x^11 * (10 * (1 - x))^6 + 6 * (10 * x)^3 *  (1 - x)^10 + 1 + sin(x * 12 * pi)
n = 100
set.seed(1234)
e <- rnorm(n, 0, sd = 0.7)
# x.test <- sort(runif(n))
x.test <- seq(0,1,length  = n)

fx <- f(x.test)
y <- fx+ e
curve(f, col = "red", ylim = range(c(y,fx)), n = 3*n)
points(x.test, y, col = "blue")
df.data <- data.frame(x.test = x.test, y = y)


fit.nb <- function(nb, x.grid =seq(0,1,l = 300) )
{
  bspl <- create.bspline.basis(norder=3, breaks=seq(from =0,to =1, length= nb-1))
  coef <- diag(rep(1, nb))
  des.mat <- eval.fd(evalarg = x.test ,  fdobj =  fd(coef, bspl))
  fitted.coef <- lm(y ~ des.mat -1)$coef
  curve(f, col = "black", ylim = range(c(y,fx)), n = 3*n)
  points(x.test, y, col = "blue")
  y.fitted <- eval.fd(x.grid,  fd(fitted.coef, bspl))
  MSE <- round(mean((f(x.grid) - y.fitted)^2), digits = 3)
  lines(x.grid, y.fitted, col = "blue")
  legend("topright", legend = paste0("MSE = ", MSE))
  return(data.frame(nbasis = nb,x.grid = x.grid, fitted = y.fitted, MSE =MSE))
}

df.plot <- do.call(rbind, lapply(seq(from = 4, to = 30, by = 2), function(z)fit.nb(nb = z)))
df.plot$nbasis <- factor(df.plot$nbasis)
df.truefun <- data.frame(x.test = sort(unique(df.plot$x.grid)), f = f( sort(unique(df.plot$x.grid))) )



plot(unique(df.plot[,c(1,4)]), type  = "l", xlab = "number of basis functions")
points(unique(df.plot[,c(1,4)])[which.min(unique(df.plot[,c(1,4)])[,2]),], pch = 20, col = "red")

anim <- animate( ggplot(df.plot, aes(x = x.grid, y = fitted)) +
                   geom_line() +
                   geom_point(data = df.data, mapping = aes(x.test, y= y, col ="observed data" )) +
                   geom_line(data = df.truefun, mapping = aes(x.test, f, col = "true function")) +
                   theme(text = element_text(size = 20)) +
                   labs(title = "Fitted function", subtitle = "Number of basis function: {closest_state}") +
                   transition_states(nbasis, transition_length = 3, state_length = 3),
                 width = 800, height = 500
)
anim_save(anim, file = "graphs/spline_fit_nbasis_anim.gif") 

gif <- image_read("graphs/spline_fit_nbasis_anim.gif")
# this is the original gif
gif2 <- image_animate(gif, loop = 2)
# gif that will loop only two times
image_write(gif2, "graphs/spline_fit_nbasis_anim_short.gif")

# save(df.plot, file = "dfPlot.RData")
