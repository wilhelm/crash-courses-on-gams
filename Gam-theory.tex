% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[
  ignorenonframetext,
]{beamer}
\usepackage{pgfpages}
\setbeamertemplate{caption}[numbered]
\setbeamertemplate{caption label separator}{: }
\setbeamercolor{caption name}{fg=normal text.fg}
\beamertemplatenavigationsymbolsempty
% Prevent slide breaks in the middle of a paragraph
\widowpenalties 1 10000
\raggedbottom
\setbeamertemplate{part page}{
  \centering
  \begin{beamercolorbox}[sep=16pt,center]{part title}
    \usebeamerfont{part title}\insertpart\par
  \end{beamercolorbox}
}
\setbeamertemplate{section page}{
  \centering
  \begin{beamercolorbox}[sep=12pt,center]{part title}
    \usebeamerfont{section title}\insertsection\par
  \end{beamercolorbox}
}
\setbeamertemplate{subsection page}{
  \centering
  \begin{beamercolorbox}[sep=8pt,center]{part title}
    \usebeamerfont{subsection title}\insertsubsection\par
  \end{beamercolorbox}
}
\AtBeginPart{
  \frame{\partpage}
}
\AtBeginSection{
  \ifbibliography
  \else
    \frame{\sectionpage}
  \fi
}
\AtBeginSubsection{
  \frame{\subsectionpage}
}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
\usetheme[]{metropolis}
\usecolortheme{dolphin}
\usefonttheme{structurebold}
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdftitle={GAM - Generalized Additive Models},
  pdfauthor={Matthieu Wilhelm},
  hidelinks,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\newif\ifbibliography
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\newcommand{\iidsim}{\stackrel{\text{i.i.d.}}{\sim}}
\newcommand{\dred}[1]{\begin{color}{red}{#1}\end{color}}
\newcommand{\dblue}[1]{\begin{color}{blue}{#1}\end{color}}
\usepackage{hyperref}
\hypersetup{
    pdftoolbar=true,        % show Acrobat’s toolbar?
    pdfmenubar=true,        % show Acrobat’s menu?
    pdffitwindow=false,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    colorlinks=true,        % false: boxed links; true: colored links
    linkcolor=blue,        % color of internal links (change box color with linkbordercolor)
    citecolor=blue,         % color of links to bibliography
    filecolor=magenta,      % color of file links
    urlcolor=blue          % color of external links
}
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\title{GAM - Generalized Additive Models}
\subtitle{Some theoretical insights}
\author{Matthieu Wilhelm}
\date{Mycology, Agroscope, Changins, 19th August 2022}

\begin{document}
\frame{\titlepage}

\hypertarget{from-lms-to-gams}{%
\section{from LMs to GAMs}\label{from-lms-to-gams}}

\begin{frame}{Some background: from linear models to GAMs}
\protect\hypertarget{some-background-from-linear-models-to-gams}{}
Given an observed variable \(Y\) and covariates \(x_1, \dots, x_p\), the
\emph{linear model} assumes the following:

\[Y = \beta_0  + x_1 \beta_1 + \dots + \beta_p x_p + \varepsilon,  \]
where \(\varepsilon\) is a random noise. In plain words,
\[Y = \text{linear combination of covariates}  + \text{noise/error}. \]
\end{frame}

\begin{frame}{Mathematically speaking}
\protect\hypertarget{mathematically-speaking}{}
By collecting the covariates associated with the \(i-\)th observation in
a vector \(\mathbf{x}_i\) and assuming that the noise is Gaussian, it
becomes:
\[Y_i  = \mathbf{x}_i^\top \boldsymbol{\beta} + \varepsilon_i, \quad \text{for all }i = 1, \dots, n, \quad \varepsilon_1, \dots, \varepsilon_n \stackrel{\text{i.i.d.}}{\sim} \mathcal{N}(0, \sigma^2), \]
for some parameter \(\sigma >0\). Equivalently, it means that
\[(Y_1, \dots, Y_n) \sim \mathcal{N}(\boldsymbol{\mu}, \sigma^2\mathbf{I}), \quad \mu_i = \mathbf{x}_i^\top \boldsymbol{\beta}\]
where \(\mathbf{I}\) is the identity matrix.
\end{frame}

\begin{frame}{So what does this mean?}
\protect\hypertarget{so-what-does-this-mean}{}
Recall that:
\[Y_i \sim \dred{\mathcal{N}}(\dblue{\mathbf{x}_i^\top \boldsymbol{\beta}}, \sigma^2), \]
We have actually made two distinct hypotheses:

\begin{itemize}
\tightlist
\item
  A \dred{distributional} hypothesis about the errors: they are
  Gaussian.
\item
  A \dblue{structural} hypothesis about the mean \(\mu_i\) of the
  observations: it is a linear combination of the covariates.
\end{itemize}
\end{frame}

\begin{frame}{From LM to Generalized linear models}
\protect\hypertarget{from-lm-to-generalized-linear-models}{}
A first generalization of linear models are the
\emph{generalized linear models} (GLM).

\begin{itemize}
\tightlist
\item
  We relax the \dred{distributional} assumption (normality): instead of
  considering the Gaussian (Normal) distribution
  \(\mathcal{N}(\mu, \sigma)\) we consider the Exponential family
  distribution\footnote<.->{A large family of distributions, including
    Gaussian, Gamma, Bernoulli, Poisson, Dirichlet, Exponential,
    \(\chi^2\); see
    \href{https://en.wikipedia.org/wiki/Exponential_family}{here} for
    details.}: \(\text{Exp Fam}(\mu, \phi )\)
\item
  We (almost) keep the \dblue{structural} hypothesis:
  \[\mu = g^{-1}(\mathbf{X}\beta)\] and the \emph{scale} parameter
  \(\phi\) is estimated or known. ---
\end{itemize}
\end{frame}

\begin{frame}{Including a gif}
\protect\hypertarget{including-a-gif}{}
\includegraphics{graphs/spline_fit_nbasis_anim.gif}
\end{frame}

\end{document}
